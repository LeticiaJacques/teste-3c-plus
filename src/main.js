import { createApp } from 'vue'
import App from './App.vue'
import "@/assets/variaveis.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";

createApp(App).mount('#app')