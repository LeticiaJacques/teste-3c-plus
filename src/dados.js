const conversasIniciais = [
    {
        "usuario": "Abner José Campos",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/account-44-102976.png",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Quero pagar minha fatura mas não consigo",
                "verde": false
            },
            {
                "horario": "12:56",
                "conteudo": "Olá Sr. Abner! Vou te ajudar a emitir um novo boleto.",
                "verde": true
            },
            {
                "horario": "12:56",
                "conteudo": "Preciso de algumas informações. Qual o número do seu CPF?",
                "verde": true
            },
            {
                "horario": "12:59",
                "conteudo": "Um momento, por favor",
                "verde": false
            },
        ]
    },
    {
        "usuario": "Elaine da Silva",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/account-53-102990.png",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Foi você que me ligou?",
                "verde": false
            },
            {
                "horario": "12:54",
                "conteudo": "Bom dia! Sim tentamos contato por ligação. Podemos agendar um horário para conversar?",
                "verde": true
            },
        ]
    },
    {
        "usuario": "Camila Aparecida",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/account-47-102980.png",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Ahh entendi!",
                "verde": true
            },
            {
                "horario": "12:54",
                "conteudo": "O que você achou do novo recurso do nosso site?!",
                "verde": false
            },
            {
                "horario": "12:54",
                "conteudo": "?!",
                "verde": false
            },

        ]
    },
    {
        "usuario": "Léia de Souza Bueno",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/account-48-102981.png",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Vou realizar o pagamento após as 15h",
                "verde": false
            },
        ]
    },
    {
        "usuario": "Júlio Zvingila",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/account-51-102985.png?",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Foto",
                "verde": false
            },
        ]
    },    
    {
        "usuario": "José Batista Souza",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/account-49-102982.png",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Arquivo",
                "verde": false
            },
        ]
    },
    {
        "usuario": "Julio Cesar Lopes",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/male-19-102989.png",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Vai fazer o pagamento hoje ainda?",
                "verde": false
            },
        ]
    },
    {
        "usuario": "José Batista Souza",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/avatar-2-102983.png",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Meu pagamento caiu aí?",
                "verde": false
            },
        ]
    },
    {
        "usuario": "Carlos Tozeli",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/account-52-102988.png",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Recebeu meu pagamento?",
                "verde": false
            },
        ]
    },
    {
        "usuario": "Grasielle Castro",
        "imagem": "https://cdn.iconscout.com/icon/premium/png-512-thumb/girl-6-102986.png",
        "mensagens": [
            {
                "horario": "12:54",
                "conteudo": "Posso mandar audio?",
                "verde": false
            },
        ]
    },
];
export default conversasIniciais;